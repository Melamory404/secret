﻿using DBRepository.Interfaces;
using FServices.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FServices.Services.Implementations
{
    public class ForumService : IForumService
    {
        IForumRepository _repository;
        readonly IConfiguration _configration;

        public ForumService(IForumRepository repository, IConfiguration configuration)
        {
            _repository = repository;
            _configration = configuration;
        }

        public async Task AddLike(Like like)
        {
            await _repository.AddLike(like);
        }

        public async Task AddSection(Section section)
        {
            await _repository.AddSection(section);
        }

        public async Task AddTopic(Topic topic)
        {
            await _repository.AddTopic(topic);
        }

        public async Task AddTopicMessage(TopicMessage topicMessage)
        {
            await _repository.AddTopicMessage(topicMessage);
        }

        public async Task DeleteSection(int sectionId)
        {
            await _repository.DeleteSection(sectionId);
        }

        public async Task DeleteTopic(int topicId)
        {
            await _repository.DeleteTopic(topicId);
        }

        public async Task DeleteTopicMessage(int topicMessageId)
        {
            await _repository.DeleteTopicMessage(topicMessageId);
        }

        public async Task<List<Like>> GetLikes(int topicMessageId)
        {
            return await _repository.GetLikes(topicMessageId);
        }

        public async Task<List<Section>> GetSections()
        {
            return await _repository.GetSections();
        }

        public async Task<Topic> GetTopic(int topicId)
        {
            return await _repository.GetTopic(topicId);
        }

        public async Task<List<TopicMessage>> GetTopicMessages(int topicId)
        {
            return await _repository.GetTopicMessages(topicId);
        }

        public async Task<List<Topic>> GetTopics()
        {
            return await _repository.GetTopics();
        }

        public async Task DeleteLike(Like like)
        {
             await _repository.DeleteLike(like);
        }

        public async Task<Like> IsLikePresent(int topicMessageId, string userId)
        {
            return await _repository.IsLikePresent(topicMessageId, userId);
        }

        public async Task DeleteConversation(int conversationId)
        {
            await _repository.DeleteConversation(conversationId);
        }

        public async Task AddConverstation(Conversation conversation)
        {
            await _repository.AddConverstation(conversation);
        }

        public async Task AddUserConverstation(UserConversation userConversation)
        {
            await _repository.AddUserConverstation(userConversation);
        }

        public async Task DeleteUserConverstation(int userConversationId)
        {
            await _repository.DeleteUserConverstation(userConversationId);
        }

        public async Task<List<Conversation>> GetConversations(string userId)
        {
            return await _repository.GetConversations(userId);
        }

        public async Task<Conversation> GetConversation(int id)
        {
            return await _repository.GetConversation(id);
        }

        public async Task AddPrivateMessage(string userId, int convId, string message)
        {
            PrivateMessage privateMessage = new PrivateMessage()
            {
                UserId = userId,
                Message = message,
                ConversationId = convId
            };
            await _repository.AddPrivateMessage(privateMessage);
        }

        public async Task<bool> IsUserConversations(string userId, int convId)
        {
            return await _repository.IsUserConversations(userId, convId);
        }

        public async Task<int> AddOrDeleteLike(int topicMessageId, string userId)
        {
            var lk = await IsLikePresent(topicMessageId, userId);

            if (lk != null)
            {
                await DeleteLike(lk);
            }
            else
            {
                Like like = new Like()
                {
                    TopicMessageId = topicMessageId,
                    UserId = userId
                };
                await AddLike(like);
            }


            var likes = await GetLikes(topicMessageId);

            return likes.Count;
        }
    }
}
