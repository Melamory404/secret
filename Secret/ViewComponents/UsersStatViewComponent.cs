﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.ViewComponents
{
    public class UsersStatViewComponent : ViewComponent
    {
        private UserManager<User> _userManager;

        public UsersStatViewComponent(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var usersCount = _userManager.Users.Count();
            var admins = await _userManager.GetUsersInRoleAsync("admin");
            var adminsCount = admins.Count();

            return new HtmlContentViewComponentResult(
                new HtmlString($"<p class=\"text-secondary\">Users: {usersCount}</p> <p class=\"text-secondary\">Admins: {adminsCount}</p>")); ;
        }
    }
}
