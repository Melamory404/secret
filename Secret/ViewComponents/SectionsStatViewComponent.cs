﻿using DBRepository.Interfaces;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.ViewComponents
{
    public class SectionsStatViewComponent : ViewComponent
    {
        private IForumRepository _forumRepository;
        public SectionsStatViewComponent(IForumRepository forumRepository)
        {
            _forumRepository = forumRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _forumRepository.GetSections();
            var count = items.Count();
            return new HtmlContentViewComponentResult(
                new HtmlString($"<p class=\"text-secondary\">Sections: {count}</p>")); ;
        }
    }
}
