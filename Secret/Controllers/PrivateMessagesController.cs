﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DBRepository;
using Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using FServices.Services.Interfaces;

namespace Secret.Views.Conversation
{
    [Authorize]
    public class PrivateMessagesController : Controller
    {
        private readonly RepositoryContext _context;
        IForumService _forumService;
        ILogger<PrivateMessagesController> _logger;
        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public PrivateMessagesController(RepositoryContext context, IForumService forumService, UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor, ILogger<PrivateMessagesController> logger)
        {
            _context = context;
            _logger = logger;
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;
        }


        public async Task<IActionResult> Create(int id)
        {
            var user = await userManager.GetUserAsync(httpContextAccessor.HttpContext.User);

            ViewData["ConversationId"] = id;
            ViewData["UserId"] = user.Id;
            return View();
        }

        // POST: PrivateMessages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Message,UserId,ConversationId")] PrivateMessage privateMessage)
        {
       //     User currentUser = await userManager.GetUserAsync(HttpContext.User);
       //     privateMessage.UserId = currentUser.Id;
            if (ModelState.IsValid)
            {
                _context.Add(privateMessage);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            ViewData["ConversationId"] = new SelectList(_context.Conversations, "Id", "Id", privateMessage.ConversationId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", privateMessage.UserId);
            return View(privateMessage);
        }
    }
}
