﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FServices.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;
using Secret.ViewModels;

namespace Secret.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        UserManager<User> _userManager;
        IForumService _forumService;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UsersController(IForumService forumService, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _forumService = forumService;
            this.httpContextAccessor = httpContextAccessor;
        }

        public IActionResult Index() => View(_userManager.Users.ToList());

        [Authorize(Roles = "admin")]
        public IActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { Email = model.Email, UserName = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            EditUserViewModel model = new EditUserViewModel { Id = user.Id, Email = user.Email};
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.Email = model.Email;
                    user.UserName = model.Email;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }

        public async Task StartConversation(string id)
        {
            var currentUser = await _userManager.GetUserAsync(httpContextAccessor.HttpContext.User);
            var toUser = await _userManager.FindByIdAsync(id);
            if (toUser != null)
            {
                Conversation conversation = new Conversation();
                await _forumService.AddConverstation(conversation);
                UserConversation userConversation = new UserConversation()
                {
                    UserId = currentUser.Id,
                    ConversationId = conversation.Id
                };

                UserConversation userConversation2 = new UserConversation()
                {
                    UserId = toUser.Id,
                    ConversationId = conversation.Id
                };

                await _forumService.AddUserConverstation(userConversation);
                await _forumService.AddUserConverstation(userConversation2);
            }
        }
    }
}