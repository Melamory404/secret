﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBRepository;
using DBRepository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Secret.Controllers
{
    [Authorize]
    public class TopicMessagesController : Controller
    {
        private readonly RepositoryContext _context;
        private readonly IForumRepository _forumRepository;
        private readonly UserManager<User> _userManager;

        public TopicMessagesController(UserManager<User> userManager, IForumRepository forumRepository, RepositoryContext context)
        {
            _userManager = userManager;
            _forumRepository = forumRepository;
            _context = context;
        }


        public IActionResult Create(int id)
        {
            ViewData["TopicId"] = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Message, TopicId")] TopicMessage topicMessage)
        {
            User currentUser = await _userManager.GetUserAsync(HttpContext.User);

            topicMessage.AuthorId = currentUser.Id;

            if (ModelState.IsValid)
            {
                await _forumRepository.AddTopicMessage(topicMessage);

                return RedirectToAction("TopicMessages", "Home", new { topicId = topicMessage.TopicId });
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var topic = await _context.TopicMessages
                .Include(t => t.Author)
                .Include(t => t.Topic)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (topic == null)
            {
                return NotFound();
            }

            return View(topic);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var topic = await _context.TopicMessages.FindAsync(id);
            _context.TopicMessages.Remove(topic);
            await _context.SaveChangesAsync();
            return RedirectToAction("TopicMessages", "Home", new { topicId = topic.TopicId });
        }

        private bool TopicMessageExists(int id)
        {
            return _context.TopicMessages.Any(e => e.Id == id);
        }

    }
}