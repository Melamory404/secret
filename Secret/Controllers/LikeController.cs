﻿using FServices.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using System.Threading.Tasks;

namespace Secret.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class LikeController : Controller
    {
        IForumService _forumService;
        ILogger<LikeController> _logger;
        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public LikeController(IForumService forumService, UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor, ILogger<LikeController> logger)
        {
            _forumService = forumService;
            _logger = logger;
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] int topicMessageId)
        {
            var user = await userManager.GetUserAsync(httpContextAccessor.HttpContext.User);

    
            var count = await _forumService.AddOrDeleteLike(topicMessageId, user.Id);

          //  var likes = await _forumService.GetLikes(topicMessageId);
            //
            return Ok(new { likes = count });
        }
    }
}
