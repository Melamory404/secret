﻿using DBRepository;
using DBRepository.Interfaces;
using FServices.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Models;
using Secret.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.Controllers
{
    public class ConversationController : Controller
    {
        private readonly IForumService _forumRepository;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHubContext<NotificationHub> notificationHub;
        ILogger<ConversationController> logger;

        public ConversationController(IHubContext<NotificationHub> notificationHub, ILogger<ConversationController> logger, UserManager<User> userManager, IForumService forumRepository, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _forumRepository = forumRepository;
            _httpContextAccessor = httpContextAccessor;
            this.notificationHub = notificationHub;
            this.logger = logger;
        }


        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            return View(await _forumRepository.GetConversations(user.Id));
        }

        public async Task<IActionResult> Detail(int id)
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            var conv = await _forumRepository.GetConversation(id);
            var haveAccess = await _forumRepository.IsUserConversations(user.Id, 325);

            if (haveAccess)
            {
                return View(conv);
            }
            else
            {
                await notificationHub.Clients.All.SendAsync("Send", "You have no access !");
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<IActionResult> AddMessage(int convId, string message)
        {
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
  
            await _forumRepository.AddPrivateMessage(user.Id, convId, message);

            var haveAccess = await _forumRepository.IsUserConversations(user.Id, 325);

            if (haveAccess)
            {
                return RedirectToAction("Detail", "Conversation", new { id = convId });
            }
            else
            {
                await notificationHub.Clients.All.SendAsync("Send", "You have no access !");
                return View();
            }
        }
    }
}
