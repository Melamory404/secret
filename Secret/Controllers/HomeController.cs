﻿using FServices.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Secret.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.Controllers
{
    public class HomeController: Controller
    {
        IForumService _forumService;
        ILogger<HomeController> _logger;

        public HomeController(IForumService forumService, ILogger<HomeController> logger)
        {
            _forumService = forumService;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Sections()
        {
            List<Section> sections = await _forumService.GetSections();
            return PartialView("_Sections", sections); ;
        }

        public async Task<IActionResult> TopicDetail(int id)
        {
            Topic topic = await _forumService.GetTopic(id);
            return View(topic);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        public async Task<IActionResult> TopicMessages(int topicId, int page = 1)
        {
            int pageSize = 5;
            var topic = await _forumService.GetTopic(topicId);
            List<TopicMessage> source = await _forumService.GetTopicMessages(topicId);
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            TopicMessagesViewModel pageViewModel = new TopicMessagesViewModel(count, page, pageSize);
            IndexTopicMessagesViewModel viewModel = new IndexTopicMessagesViewModel
            {
                PageViewModel = pageViewModel,
                TopicMessages = items,
                Topic = topic
            };

            return View("TopicDetail", viewModel);
        }
    }
}
