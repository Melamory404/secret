﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DBRepository;
using Models;
using Microsoft.AspNetCore.Authorization;
using DBRepository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Secret.Hubs;

namespace Secret.Controllers
{
    [Authorize]
    public class TopicsController : Controller
    {
        private readonly RepositoryContext _context;
        private readonly IForumRepository _forumRepository;
        private readonly UserManager<User> _userManager;
        IHubContext<NotificationHub> notificationHub;

        public TopicsController(IHubContext<NotificationHub> notificationHub, UserManager<User> userManager, IForumRepository forumRepository, RepositoryContext context)
        {
            _userManager = userManager;
            _forumRepository = forumRepository;
            _context = context;
            this.notificationHub = notificationHub;
        }

        public async Task<IActionResult> Index()
        {
            var repositoryContext = _context.Topics.Include(t => t.Author).Include(t => t.Section);
            return View(await repositoryContext.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var topic = await _context.Topics
                .Include(t => t.Author)
                .Include(t => t.Section)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (topic == null)
            {
                return NotFound();
            }

            return View(topic);
        }

        public IActionResult Create(int id)
        {
            ViewData["SectionId"] = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name, SectionId")] Topic topic)
        {
            User currentUser = await _userManager.GetUserAsync(HttpContext.User);

            topic.AuthorId = currentUser.Id;

            if (ModelState.IsValid)
            {
                await _forumRepository.AddTopic(topic);

                return RedirectToAction("Index", "Home");
            }
          
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = ("admin"))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                await notificationHub.Clients.All.SendAsync("Send", "Bad Request");
                return BadRequest();
            }

            var topic = await _context.Topics.FindAsync(id);
            if (topic == null)
            {
                await notificationHub.Clients.All.SendAsync("Send", "Could not find topic");
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.Users, "Id", "Id", topic.AuthorId);
            ViewData["SectionId"] = new SelectList(_context.Sections, "Id", "Id", topic.SectionId);
            return View(topic);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,AuthorId,SectionId")] Topic topic)
        {
            if (id != topic.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(topic);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TopicExists(topic.Id))
                    {
                        await notificationHub.Clients.All.SendAsync("Send", "Could not find topic");
                        return NotFound();
                    }
                    else
                    {
                        await notificationHub.Clients.All.SendAsync("Send", "Topic was already changed");
                        return BadRequest();
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Users, "Id", "Id", topic.AuthorId);
            ViewData["SectionId"] = new SelectList(_context.Sections, "Id", "Id", topic.SectionId);
            return View(topic);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                await notificationHub.Clients.All.SendAsync("Send", "Bad Request");
                return BadRequest();
            }

            var topic = await _context.Topics
                .Include(t => t.Author)
                .Include(t => t.Section)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (topic == null)
            {
                await notificationHub.Clients.All.SendAsync("Send", "Could not find topic");
                return NotFound();
            }

            return View(topic);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var topic = await _context.Topics.FindAsync(id);
            _context.Topics.Remove(topic);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Sections", new { id = topic.SectionId });
        }

        private bool TopicExists(int id)
        {
            return _context.Topics.Any(e => e.Id == id);
        }
    }
}
