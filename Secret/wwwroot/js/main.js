﻿$(document).ready(function () {
    $('.user-comment').each(function (index, value) {
        $(this).html(parseBbcode($(this).html()));
    });

    let hubUrl = 'https://localhost:44309/chat';
    const hubConnection = new signalR.HubConnectionBuilder()
        .withUrl(hubUrl)
        .configureLogging(signalR.LogLevel.Information)
        .build();

    hubConnection.on("Send", function (message, userName) {

        let userNameElem = document.createElement("b");
        userNameElem.appendChild(document.createTextNode(userName + ': '));

        let elem = document.createElement("p");
        elem.appendChild(userNameElem);
        elem.appendChild(document.createTextNode(message));

        var firstElem = document.getElementById("chatroom").firstChild;
        document.getElementById("chatroom").insertBefore(elem, firstElem);

    });

    document.getElementById("sendBtn").addEventListener("click", function (e) {
        let message = document.getElementById("message").value;
        hubConnection.invoke("Send", message);
    });

    hubConnection.start();


    let notificationUrl = 'https://localhost:44309/notification';
    const notificationConnection = new signalR.HubConnectionBuilder()
        .withUrl(notificationUrl)
        .configureLogging(signalR.LogLevel.Information)
        .build();

    notificationConnection.on("Send", function (message) {

        alert(message);

    });

    notificationConnection.start();

});


invoke = (event) => {
    const BACKEND = 'https://localhost:44309/api/';

    let arg = event.getAttribute('message-id');
    sendRequest("POST", BACKEND + "Like", null, arg)
        .then(response => {
            if (response.ok) {
                return response.json();
            }
        }).then(value => {
            $('.' + arg).html(value.likes);
        });

};

function parseBbcode(content) {
    content = content.replace(/(\A|[^=\]'\"a-zA-Z0-9])((http|ftp|https|ftps|irc):\/\/[^<>\s]+)/gi, "$1<a href=$2 target=_blank>$2</a>");
    content = content.replace(/\[b\]((\s|.)+?)\[\/b\]/gi, '<span class="bold">$1</span>');
    content = content.replace(/\[i\]((\s|.)+?)\[\/i\]/gi, '<span class="italic">$1</span>');
    content = content.replace(/\[u\]((\s|.)+?)\[\/u\]/gi, '<span class="underline">$1</span>');
    content = content.replace(/\[s\]((\s|.)+?)\[\/s\]/gi, '<span class="strike">$1</span>');
    content = content.replace(/\[quote\]((\s|.)+?)\[\/quote\]/gi, '<blockquote>$1</blockquote>');
    content = content.replace(/\[center\]((\s|.)+?)\[\/center\]/gi, '<span class="text-align align--center">$1</span>');
    content = content.replace(/\[align=((center|left|right|justify):\/\/[^<>\s]+?)\)]((\s|.)+?)\[\/align\]/gi, '<span class="text-align align--$1">$2</span>');
    content = content.replace(/\[url=((http|ftp|https|ftps|irc):\/\/[^<>\s]+?)\]((\s|.)+?)\[\/url\]/gi, "<a href=\"$1\" target=\"_blank\">$3</a>");
    content = content.replace(/\[url\]((http|ftp|https|ftps|irc):\/\/[^<>\s]+?)\[\/url\]/gi, "<a href=\"$1\" target=\"_blank\">$1</a>");
    content = content.replace(/\[img\]((\s|.)+?)\[\/img\]/gi, '<img src="$1" alt="">');
    content = content.replace(/\n/g, "<br/>");
    return content;
}


const setHeader = token => ({
    'Content-Type': 'application/json',
    'Authorization': token
});

const sendRequest = async (method, url, token, data) => {
    let optionsObj = {};

    if (data) {
        optionsObj = {
            method: `${method}`,
            headers: setHeader(token),
            body: JSON.stringify(data)
        };
    } else {
        optionsObj = {
            method: `${method}`,
            headers: setHeader(token)
        };
    }

    const response = await fetch(url, optionsObj);

    return response;
};