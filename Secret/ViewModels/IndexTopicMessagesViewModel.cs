﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.ViewModels
{
    public class IndexTopicMessagesViewModel
    {
        public List<TopicMessage> TopicMessages { get; set; }
        public TopicMessagesViewModel PageViewModel { get; set; }
        public Topic Topic { get; set; }
    }
}
