﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.Hubs
{
    public class ChatHub : Hub
    {

        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public ChatHub(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;

        }

        public async Task Send(string message)
        {
            var user = await userManager.GetUserAsync(httpContextAccessor.HttpContext.User);

            await Clients.All.SendAsync("Send", message, user.UserName);
        }
    }
}
