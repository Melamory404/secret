﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.TagHelpers
{
    [HtmlTargetElement("user", TagStructure = TagStructure.NormalOrSelfClosing)]
    public class UserTagHelper : TagHelper
    {
        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UserTagHelper(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;

        }

        public async override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";

            var user = await userManager.GetUserAsync(httpContextAccessor.HttpContext.User);
            if (user == null)
            {
                output.Attributes.SetAttribute("Style", "visibility:hidden");
            }
        }
    }
}
