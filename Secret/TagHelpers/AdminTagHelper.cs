﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Secret.TagHelpers
{
    [HtmlTargetElement("admin", TagStructure = TagStructure.NormalOrSelfClosing)]
    public class AdminTagHelper : TagHelper
    {
        private readonly UserManager<User> userManager;
        private readonly IHttpContextAccessor httpContextAccessor;

        public AdminTagHelper(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.userManager = userManager;
            this.httpContextAccessor = httpContextAccessor;

        }

        public async override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";

            var user = await userManager.GetUserAsync(httpContextAccessor.HttpContext.User);
            if (user == null)
            {
                output.Attributes.SetAttribute("Style", "visibility:hidden");
                return;
            }
            var currentRoles = await userManager.GetRolesAsync(user);

            if (!currentRoles.Contains("admin"))
            {
                output.Attributes.SetAttribute("Style", "visibility:hidden");
            }
        }
    }
}
