﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class TopicMessage
    {
        public int Id { get; set; }

        public string Message { get; set; }

        public string AuthorId { get; set; }
        public User Author { get; set; }
    
        public List<Like> Likes { get; set; }

        public int TopicId { get; set; }
        public Topic Topic { get; set; }
    }
}
