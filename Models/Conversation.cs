﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Conversation
    {
        public int Id { get; set; }

        public List<UserConversation> UserConversations  { get; set; }

        public List<PrivateMessage> PrivateMessages { get; set; }
    }
}
