﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Like
    {
        public int Id { get; set; }

        public int TopicMessageId { get; set; }
        public TopicMessage TopicMessage { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }
    }
}
