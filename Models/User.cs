﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class User : IdentityUser
    {
        public List<Topic> Topics { get; set; }

        public List<UserConversation> UserConversations { get; set; }

        public List<PrivateMessage> PrivateMessages { get; set; }
    }
}
