﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Topic
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string AuthorId { get; set; }
        public User Author { get; set; }

        public int SectionId { get; set; }
        public Section Section { get; set; }

        public List<TopicMessage> TopicMessages { get; set; }
    }
}
