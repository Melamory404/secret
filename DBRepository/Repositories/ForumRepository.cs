﻿using DBRepository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRepository.Repositories
{

    public class ForumRepository : BaseRepository, IForumRepository
    {
        private RepositoryContext context;
        private UserManager<User> _userManager;
        public ForumRepository(string connectionString, IRepositoryContextFactory contextFactory, UserManager<User> userManager)
            : base(connectionString, contextFactory)
        {
            context = ContextFactory.CreateDbContext(ConnectionString);
            _userManager = userManager;
        }

        public async Task AddSection(Section section)
        {
           if (section != null)
           {
               await context.Sections.AddAsync(section);
               await context.SaveChangesAsync();
           }
        }

        public async Task AddTopic(Topic topic)
        {
                if (topic != null)
                {
                    await context.Topics.AddAsync(topic);
                    await context.SaveChangesAsync();
                }
        }

        public async Task AddTopicMessage(TopicMessage topicMessage)
        {
                if (topicMessage != null)
                {
                    await context.TopicMessages.AddAsync(topicMessage);
                    await context.SaveChangesAsync();
                }
        }

        public async Task DeleteSection(int sectionId)
        {
                var section = await context.Sections.FindAsync(sectionId);

                if (section != null)
                {
                    context.Sections.Remove(section);
                    await context.SaveChangesAsync();
                }
        }

        public async Task DeleteTopic(int topicId)
        {
                var topic = await context.Topics.FindAsync(topicId);

                if (topic != null)
                {
                    context.Topics.Remove(topic);
                    await context.SaveChangesAsync();
                }
        }

        public async Task DeleteTopicMessage(int topicMessageId)
        {
                var topicMessage = await context.TopicMessages.FindAsync(topicMessageId);

                if (topicMessage != null)
                {
                    context.TopicMessages.Remove(topicMessage);
                    await context.SaveChangesAsync();
                }
        }

        public async Task<List<Section>> GetSections()
        {
                return await context.Sections.Include(section => section.Topics).ToListAsync();
        }

        public async Task<Topic> GetTopic(int topicId)
        {
                return await context.Topics.Include("TopicMessages.Author")
                    .Include("TopicMessages.Likes")
                    .SingleAsync(topic => topic.Id == topicId);
        }

        public async Task<List<Like>> GetLikes(int topicMessageId)
        {
            return await context.Likes.Where(like => like.TopicMessageId == topicMessageId).ToListAsync();
        }

        public async Task AddLike(Like like)
        {
            if (like != null)
            {
                context.Likes.Add(like);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Topic>> GetTopics()
        {
                return await context.Topics.ToListAsync();
        }

        public async Task<List<TopicMessage>> GetTopicMessages(int topicId)
        {
            var topic = await context.Topics.FindAsync(topicId);

            if (topic != null)
            {
                return topic.TopicMessages;
            }
            else
            {
                return null;
            }
        }

        public Task<Like> IsLikePresent(int topicMessageId, string userId)
        {
            return context.Likes.SingleOrDefaultAsync(lk => lk.TopicMessageId == topicMessageId && lk.UserId == userId);
        }

        public async Task DeleteLike(Like like)
        {
            if (like != null)
            {
                context.Likes.Remove(like);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteConversation(int conversationId)
        {
            var conversation = await context.Conversations.FindAsync(conversationId);

            if (conversation != null)
            {
                context.Conversations.Remove(conversation);
                await context.SaveChangesAsync();
            }
        }

        public async Task AddConverstation(Conversation conversation)
        {
            if (conversation != null)
            {
                context.Conversations.Add(conversation);
                await context.SaveChangesAsync();
            }
        }

        public async Task AddUserConverstation(UserConversation userConversation)
        {
            if (userConversation != null)
            {
                context.UserConversations.Add(userConversation);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteUserConverstation(int userConversationId)
        {
            var userConversation = await context.UserConversations.FindAsync(userConversationId);

            if (userConversation != null)
            {
                context.UserConversations.Remove(userConversation);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Conversation>> GetConversations(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                return await context.Conversations.Where(conv => conv.UserConversations.Any(USV => USV.UserId == userId)).ToListAsync();
            }
            else
            {
                return null;
            }
        }

        public async Task<Conversation> GetConversation(int id)
        {
            return await context.Conversations.Include("PrivateMessages.User").SingleAsync(conv => conv.Id == id);
        }

        public async Task AddPrivateMessage(PrivateMessage privateMessage)
        {
            if (privateMessage != null)
            {
                await context.PrivateMessages.AddAsync(privateMessage);
                await context.SaveChangesAsync();
            }
        }

        public async Task<bool> IsUserConversations(string userId, int convId)
        {
            return await context.UserConversations.Where(usv => usv.UserId == userId && usv.ConversationId == convId).CountAsync() > 0;
        }
    }
}
