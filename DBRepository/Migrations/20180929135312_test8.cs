﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBRepository.Migrations
{
    public partial class test8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Likes_AspNetUsers_UserId1",
                table: "Likes");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id1",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id1",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_TopicMessages_AspNetUsers_AuthorId1",
                table: "TopicMessages");

            migrationBuilder.DropIndex(
                name: "IX_TopicMessages_AuthorId1",
                table: "TopicMessages");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_User1Id1",
                table: "PrivateMessages");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_User2Id1",
                table: "PrivateMessages");

            migrationBuilder.DropIndex(
                name: "IX_Likes_UserId1",
                table: "Likes");

            migrationBuilder.DropColumn(
                name: "AuthorId1",
                table: "TopicMessages");

            migrationBuilder.DropColumn(
                name: "User1Id1",
                table: "PrivateMessages");

            migrationBuilder.DropColumn(
                name: "User2Id1",
                table: "PrivateMessages");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Likes");

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "TopicMessages",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "User2Id",
                table: "PrivateMessages",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "User1Id",
                table: "PrivateMessages",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Likes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_TopicMessages_AuthorId",
                table: "TopicMessages",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_User1Id",
                table: "PrivateMessages",
                column: "User1Id");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_User2Id",
                table: "PrivateMessages",
                column: "User2Id");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_UserId",
                table: "Likes",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_AspNetUsers_UserId",
                table: "Likes",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id",
                table: "PrivateMessages",
                column: "User1Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id",
                table: "PrivateMessages",
                column: "User2Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TopicMessages_AspNetUsers_AuthorId",
                table: "TopicMessages",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Likes_AspNetUsers_UserId",
                table: "Likes");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_TopicMessages_AspNetUsers_AuthorId",
                table: "TopicMessages");

            migrationBuilder.DropIndex(
                name: "IX_TopicMessages_AuthorId",
                table: "TopicMessages");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_User1Id",
                table: "PrivateMessages");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_User2Id",
                table: "PrivateMessages");

            migrationBuilder.DropIndex(
                name: "IX_Likes_UserId",
                table: "Likes");

            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "TopicMessages",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuthorId1",
                table: "TopicMessages",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "User2Id",
                table: "PrivateMessages",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "User1Id",
                table: "PrivateMessages",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "User1Id1",
                table: "PrivateMessages",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "User2Id1",
                table: "PrivateMessages",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Likes",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Likes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TopicMessages_AuthorId1",
                table: "TopicMessages",
                column: "AuthorId1");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_User1Id1",
                table: "PrivateMessages",
                column: "User1Id1");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_User2Id1",
                table: "PrivateMessages",
                column: "User2Id1");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_UserId1",
                table: "Likes",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_AspNetUsers_UserId1",
                table: "Likes",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id1",
                table: "PrivateMessages",
                column: "User1Id1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id1",
                table: "PrivateMessages",
                column: "User2Id1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TopicMessages_AspNetUsers_AuthorId1",
                table: "TopicMessages",
                column: "AuthorId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
