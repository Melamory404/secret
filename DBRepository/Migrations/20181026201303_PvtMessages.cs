﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBRepository.Migrations
{
    public partial class PvtMessages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id",
                table: "PrivateMessages");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_User1Id",
                table: "PrivateMessages");

            migrationBuilder.RenameColumn(
                name: "User2Id",
                table: "PrivateMessages",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "User1Id",
                table: "PrivateMessages",
                newName: "ConversationId");

            migrationBuilder.RenameIndex(
                name: "IX_PrivateMessages_User2Id",
                table: "PrivateMessages",
                newName: "IX_PrivateMessages_UserId");

            migrationBuilder.AlterColumn<string>(
                name: "ConversationId",
                table: "PrivateMessages",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConversationId1",
                table: "PrivateMessages",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Conversations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conversations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserConversation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    ConversationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserConversation", x => new { x.UserId, x.ConversationId });
                    table.ForeignKey(
                        name: "FK_UserConversation_Conversations_ConversationId",
                        column: x => x.ConversationId,
                        principalTable: "Conversations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserConversation_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_ConversationId1",
                table: "PrivateMessages",
                column: "ConversationId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserConversation_ConversationId",
                table: "UserConversation",
                column: "ConversationId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_Conversations_ConversationId1",
                table: "PrivateMessages",
                column: "ConversationId1",
                principalTable: "Conversations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_UserId",
                table: "PrivateMessages",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_Conversations_ConversationId1",
                table: "PrivateMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_UserId",
                table: "PrivateMessages");

            migrationBuilder.DropTable(
                name: "UserConversation");

            migrationBuilder.DropTable(
                name: "Conversations");

            migrationBuilder.DropIndex(
                name: "IX_PrivateMessages_ConversationId1",
                table: "PrivateMessages");

            migrationBuilder.DropColumn(
                name: "ConversationId1",
                table: "PrivateMessages");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "PrivateMessages",
                newName: "User2Id");

            migrationBuilder.RenameColumn(
                name: "ConversationId",
                table: "PrivateMessages",
                newName: "User1Id");

            migrationBuilder.RenameIndex(
                name: "IX_PrivateMessages_UserId",
                table: "PrivateMessages",
                newName: "IX_PrivateMessages_User2Id");

            migrationBuilder.AlterColumn<string>(
                name: "User1Id",
                table: "PrivateMessages",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrivateMessages_User1Id",
                table: "PrivateMessages",
                column: "User1Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User1Id",
                table: "PrivateMessages",
                column: "User1Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateMessages_AspNetUsers_User2Id",
                table: "PrivateMessages",
                column: "User2Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
