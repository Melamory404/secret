﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRepository.Interfaces
{
    public interface IForumRepository
    {
        Task<List<Section>> GetSections();
        Task AddSection(Section section);
        Task DeleteSection(int sectionId);

        Task<List<Topic>> GetTopics();
        Task AddTopic(Topic topic);
        Task DeleteTopic(int topicId);
        Task<Topic> GetTopic(int topicId);

        Task AddLike(Like like);
        Task DeleteLike(Like like);
        Task<List<Like>> GetLikes(int topicMessageId);
        Task<Like> IsLikePresent(int topicMessageId, string userId);

        Task<List<TopicMessage>> GetTopicMessages(int topicId);
        Task AddTopicMessage(TopicMessage topicMessage);
        Task DeleteTopicMessage(int topicMessageId);

        Task DeleteConversation(int conversationId);
        Task AddConverstation(Conversation conversation);
        Task<List<Conversation>> GetConversations(string userId);
        Task<Conversation> GetConversation(int id);

        Task AddUserConverstation(UserConversation userConversation);
        Task DeleteUserConverstation(int userConversationId);

        Task AddPrivateMessage(PrivateMessage privateMessage);

        Task<bool>  IsUserConversations(string userId, int convId);
    }
}
