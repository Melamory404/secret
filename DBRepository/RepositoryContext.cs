﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRepository
{
    public class RepositoryContext : IdentityDbContext<User>
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options) : base(options)
        {
            //    Database.EnsureCreated();
        }

        public DbSet<Section> Sections { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<PrivateMessage> PrivateMessages { get; set; }
        public DbSet<TopicMessage> TopicMessages { get; set; }
        public DbSet<Conversation> Conversations { get; set; }
        public DbSet<UserConversation> UserConversations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserConversation>()
           .HasKey(t => new { t.UserId, t.ConversationId });

            modelBuilder.Entity<UserConversation>()
            .HasOne(pt => pt.Conversation)
            .WithMany(p => p.UserConversations)
            .HasForeignKey(pt => pt.ConversationId);

            modelBuilder.Entity<UserConversation>()
            .HasOne(pt => pt.User)
            .WithMany(p => p.UserConversations)
            .HasForeignKey(pt => pt.UserId);


            modelBuilder.Entity<PrivateMessage>().HasOne(pvt => pvt.Conversation)
                .WithMany(conv => conv.PrivateMessages)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<PrivateMessage>().HasOne(pvt => pvt.User)
                .WithMany(usr => usr.PrivateMessages)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Topic>().HasOne(topic => topic.Author)
               .WithMany(user => user.Topics)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Section>().HasMany(section => section.Topics)
                .WithOne(topic => topic.Section).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Topic>().HasMany(topic => topic.TopicMessages)
                .WithOne(topicMsg => topicMsg.Topic)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TopicMessage>().HasMany(topic => topic.Likes)
                .WithOne(like => like.TopicMessage)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>().Property(u => u.UserName).IsRequired();
            modelBuilder.Entity<User>().Property(u => u.PasswordHash).IsRequired();
        }
    }
}
